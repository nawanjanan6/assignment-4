#include <stdio.h>

int main()
{
    int x=0, total = 0;

    do {
        printf("Enter a integer : ");
        scanf("%d", &x);
        if(x <= 0){
            break;
        }
        total += x;
    }
    while (x > 0);{
        printf("Total = %d", total);
    }

    return 0;
}
