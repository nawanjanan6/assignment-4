#include <stdio.h>

int main()
{
    int num=0, rev=0, temp=0;
    printf("Enter a number : ");
    scanf("%d", &num);

    while(num!=0){
        temp = num%10;
        num = num/10;
        rev = (rev*10) + temp;
    }

    printf("Revers > %d \n", rev);

    return 0;
}
