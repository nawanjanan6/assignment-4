#include <stdio.h>

int main()
{
    int num=0, fact=0;
    printf("Enter the number : ");
    scanf("%d", &num);

    printf("Factors are \n");

    for(int i=1; i<=num; i++){
        if(num%i==0){
           printf("%d, ", i);
        }
    }
    printf("\b\b \n");

    return 0;
}
